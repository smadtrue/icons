# Readme Smadtrue Icons #
Open-source CSS, SVG and Figma UI Icons
Available in SVG Sprite, styled-components, NPM & API

# New in 2.0
#### 🥳 200 New Icons
#### 🚀 SVG Icons
#### 🔥 SVG Sprite
#### 💅 Styled Components
#### ⚛️ React Local Styled Components
#### 🦄 Figma Components
#### 🔮 Adobe XD Components

# Table of Contents
* [Get Started](#get-started)
* [HTML include](#html-include)
  * [1. All icons](#1-all-icons)
  * [2. Single icon](#2-single-icon)
  * [3. Collection](#3-collection)
  * [4. Markup](#4-markup)
  * [5. Example](#5-example)
* [CSS @import](#css--import)
  * [1. All icons](#1-all-icons-1)
  * [2. Single icon](#2-single-icon-1)
  * [3. Collection](#3-collection-1)
  * [4. Resizing](#4-resizing)
  * [5. Coloring](#5-coloring)
* [SVG](#svg)
  * [1. SVG Sprite - Download Path](#1-svg-sprite---download-path)
    * [1.1. Example](#11-example)
  * [2. SVG Single Icon - Download Path](#2-svg-single-icon---download-path)
    * [2.1. Example - SVG Sprite](#21-example---svg-sprite)
    * [2.2. Example - Inline SVG Sprite/Symbol](#22-example---inline-svg-sprite-symbol)
    * [2.3. Example Single copy/paste icon](#23-example-single-copy-paste-icon)
  * [3. Encode SVG for CSS](#3-encode-svg-for-css)
    * [3.1. Example - Encoded SVG for CSS icon](#31-example---encoded-svg-for-css-icon)
  * [4. Coloring a SVG icon](#4-coloring-a-svg-icon)
    * [4.1. Directly on the icon](#41-directly-on-the-icon)
    * [4.2. using class](#42-using-class)
* [JSON - paths](#json---paths)
  * [1. All icons](#1-all-icons-2)
  * [2. Single icon](#2-single-icon-2)
  * [3. Collection](#3-collection-2)
* [XML - paths](#xml---paths)
  * [1. All icons](#1-all-icons-3)
  * [2. Single icon](#2-single-icon-3)
  * [3. Collection](#3-collection-3)
* [React](#react)
  * [1. Styled Components](#1-styled-components---)
  * [2. Local Single Icon as `styled-component`](#2-local-single-icon-as--styled-component-)
    * [2.1. Copy directly from the page](#21-copy-directly-from-the-page)
    * [2.2. Local Component Example](#22-local-component-example)
  * [3. SVG Sprite method](#3-svg-sprite-method)
  * [4. All CSS icons](#4-all-css-icons)
    * [4.1. Single CSS icon](#41-single-css-icon)
  * [5. All SCSS icons](#5-all-scss-icons)
    * [5.1. Single SCSS icon](#51-single-scss-icon)
  * [6. Combined import SVG, CSS, SCSS etc.](#6-combined-import-svg-css-scss-etc)
* [Design Tools](#design-tools)
  * [1. Figma](#1-figma---httpscssggfig)
  * [2. Adobe XD](#2-adobe-xd---httpscssggxd)
* [Contributors](#contributors)
* [Donate](#donate)
* [Support](#support)
* [v.1.0.6 or older](#v106-or-older)
* [TODO:](#todo)
